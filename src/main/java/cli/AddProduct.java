package cli;

import domain.Product;

import java.util.Scanner;

public class AddProduct {
    static void addProduct(Scanner sc) {
        String answerAddProduct = null;
        do {
            System.out.println("Voulez-vous ajouter un produit à votre panier ? O/N");
            answerAddProduct = sc.nextLine();

            if (answerAddProduct.equals("O")||answerAddProduct.equals("o")) {

                int productChoiceConsumer;

                System.out.println("Entrer l'id du produit");
                productChoiceConsumer = Integer.parseInt(sc.nextLine());

                Product selectedProduct = CliApplication.products.get(productChoiceConsumer - 1);//remet au niveau index
                CliApplication.basket.addProduct(selectedProduct);

            }
        } while (answerAddProduct.equals("O")||answerAddProduct.equals("o"));
    }
}
