package cli;

import domain.Product;
import domain.Basket;

import java.util.List;
import java.util.Scanner;

public class CliApplication {
    static Basket basket = new Basket();

    static List<Product> products = List.of(
            new Product(1, "farine", 12),
            new Product(2, "lait", 24),
            new Product(3, "chocolat", 30)
    );


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        for (Product product : products) {
            product.display();
        }
        basket.visualizeOrder();
        AddProduct.addProduct(sc);

        basket.visualizeOrder();

        RemoveProduct.removeProduct(sc);

        // demander si visualisation du panier
        basket.visualizeOrder();

        // demander pour finalisation du panier
        System.out.println("Finaliser la commande");
        basket.finalizeOrder();


    }


}

