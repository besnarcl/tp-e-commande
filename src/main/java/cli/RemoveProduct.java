package cli;

import domain.Product;

import java.util.Scanner;

public class RemoveProduct {
    static void removeProduct(Scanner sc) {
        String answerRemoveProduct = null;
        do {
            System.out.println("Voulez-vous retirer un produit à votre panier ? O/N");
            answerRemoveProduct = sc.nextLine();

            if (answerRemoveProduct.equals("O")||answerRemoveProduct.equals("o")) {

                String productChoiceConsumer;

                System.out.println("Entrer le nom du produit que vous souhaitez retirer : ");
                productChoiceConsumer = sc.nextLine();

                int indexProductChoose = CliApplication.basket.getProducts().indexOf(productChoiceConsumer);//find index of productChoiceConsumer
                Product selectedProduct = CliApplication.basket.getProducts().get(indexProductChoose+1);//get product choose in list
                CliApplication.basket.removeProduct(selectedProduct);

            }
        } while (answerRemoveProduct.equals("O")||answerRemoveProduct.equals("o"));
    }
}
