package domain;

import java.util.ArrayList;
import java.util.List;

public class Basket {
    private List<Product> products = new ArrayList<>();
    int totalPrice = 0;

    public List<Product> getProducts() {
        return products;
    }

    public void addProduct(Product product) {
        products.add(product);
        System.out.println("Le produit " + product.getName() + " a été ajouté au panier.");
        totalPrice += product.getPrice();
        System.out.println(totalPrice + " €");

    }

    public void removeProduct(Product product) {
        products.remove(product);
        System.out.println("Le produit " + product.getName() + " a été retiré du panier.");
        System.out.println("Prix du panier : " + (totalPrice -= product.getPrice()));
    }

    public void visualizeOrder() {
        //visualize domain.Basket
        System.out.println("Visualisation du panier : ");
        for (Product p : products) {
            System.out.println(p.toString());
        }
    }

    public void finalizeOrder() {
        System.out.println("Validation du panier : ");
        for(Product p : products) {
            System.out.println(p.toString());
        }
        System.out.println("Prix du panier : " + totalPrice + "€");
    }

}
