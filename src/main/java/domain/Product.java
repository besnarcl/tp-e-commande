package domain;

import java.util.Objects;

public class Product {
   private int id;
    private String name;
    private int price;


    public Product(int id, String name, int price) {
       this.id = id;
       if(name.equals("")){
           throw new IllegalArgumentException("Nom invalide !");
       }
        this.name = name;
        if(price<0){
            throw new IllegalArgumentException("Prix négatif !") ;
        }
        this.price = price;
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void display(){
        System.out.println(id + ". " + name + " ---> " + price + " €");
    }

    @Override
    public String toString() {
        return name + " ---> " + price + " €";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getPrice() == product.getPrice() && Objects.equals(getName(), product.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPrice());
    }
}

