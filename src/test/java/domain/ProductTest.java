package domain;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    @Test
    void priceMustBePositive() {
        try {
            new Product(1, "farine", -3);
            Assertions.fail(); //créer une erreur même si prix est positif
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    void emptyName() {
        try{
            new Product(1,"",10);
            Assertions.fail();
        } catch(IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}