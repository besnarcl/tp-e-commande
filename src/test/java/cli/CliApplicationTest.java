package cli;

import domain.Basket;
import domain.Product;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

class CliApplicationTest {
    static Basket basket = new Basket();

    static List<Product> products = List.of(
            new Product(1, "farine", 12),
            new Product(2, "lait", 24),
            new Product(3, "chocolat", 30));

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);


        for (Product product : products) {
            product.display();
        }

        //basket.addProduct();
        //basket.addProduct("lait");

        //basket.visualizeOrder();
    }
}

